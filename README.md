# Contacts Management


## Auteur

Maxime POULAIN - ISIMA ZZ2 F2

## Fonctionnalités

### Obligatoires

* socle applicatif
* persistance binaire avec le DataContract
* persistance xml avec le DataContract
* protection par chiffrement
* ajouter un contact
* ajouter un dossier
* affichage de l'arborescence

### Optionnels  

* gestion de la validité de l'email
* sélection du dossier courant voulu
* fichier de contacts enregistré dans "Mes Documents"
* protection par mot de passe et destruction du fichier au bout de 3 tentatives infructueuses
* détection des erreurs utilisateur et proposition de la commande valide la plus proche
* fichier de configuration pour la localisation de la base de données

<img src="https://i.ibb.co/X5TFPzk/image-2022-01-03-172046.png" height="100px"/>
<img src="https://i.ibb.co/7VLwcnh/image-2022-01-03-172511.png" height="100px"/>




## Usage 

Listes des commandes : 

```
addcontact
addfolder
exit
load
print
save
select
help
```

### addcontact

Ajoute un contact au dossier selectionné. 

```addcontact [name] [surname] [email] [compagny] (relation)```

### addfolder

Ajoute un dossier au dossier selectionné.

```addfolder [name]```

### exit

Quitte l'application proprement.

```exit```

### load

Charge un fichier de contacts, XML ou binaire, XML étant la méthode par défault.

```load (method)```

### print

Affiche toute l'arborescence des contacts.

```print```

### save

Sauvegarde dans un fichier les contacts de l'arborescence, XML ou binaire, XML étant la méthode par défault.

```save (method)```


### select

Séléctionne le dossier voulu. Si aucun dossier de se nom est trouvé, le dossier selectionné ne change pas.

```select [name]```

### help

Affiche l'aide.

```help```

## Éxecution

### Sans erreur

<img src="https://i.ibb.co/JjLNNmC/image-2022-01-03-171414.png"/>

### Avec erreurs

<img src="https://i.ibb.co/zHx9fkR/image-2022-01-03-171828.png"/>


## Diagramme de paquets

L'architecture de l'application est la suivante : 

<img src="https://i.ibb.co/MN952K1/image-2022-01-04-154734.png" height="300px"/>

Utilisation d'un patron composite pour l'arborescence des contacts.
Utilisation d'un patron factory pour la sérialisation.

## Persistance  

La gestion de la persistance, disponible en XML et en binaire.


### XML

Exemple de persistance XML : 

```xml
<?xml version="1.0" encoding="utf-8"?>
<Component xmlns:i="http://www.w3.org/2001/XMLSchema-instance" i:type="Folder" xmlns="http://schemas.datacontract.org/2004/07/Model.Composite">
  <Creation>2022-01-03T12:58:10.2568634+01:00</Creation>
  <Depth>0</Depth>
  <Modification>2022-01-03T13:01:16.8758656+01:00</Modification>
  <Name>Root</Name>
  <Components>
    <Component i:type="Contact">
      <Creation>2022-01-03T12:59:39.795359+01:00</Creation>
      <Depth>1</Depth>
      <Modification>2022-01-03T12:59:39.7953657+01:00</Modification>
      <Name>POULAIN</Name>
      <Company>Isima</Company>
      <Email>Maxime.POULAIN@etu.uca.fr</Email>
      <Relation>Other</Relation>
      <Surname>Maxime</Surname>
    </Component>
    <Component i:type="Contact">
      <Creation>2022-01-03T13:00:50.9634656+01:00</Creation>
      <Depth>1</Depth>
      <Modification>2022-01-03T13:00:50.9634816+01:00</Modification>
      <Name>LY</Name>
      <Company>Isima</Company>
      <Email>Sinaro.LY@etu.uca.fr</Email>
      <Relation>Other</Relation>
      <Surname>Sinaro</Surname>
    </Component>
    <Component i:type="Folder">
      <Creation>2022-01-03T13:01:16.875501+01:00</Creation>
      <Depth>1</Depth>
      <Modification>2022-01-03T13:03:44.0061039+01:00</Modification>
      <Name>Copains</Name>
      <Components>
        <Component i:type="Contact">
          <Creation>2022-01-03T13:03:44.0060545+01:00</Creation>
          <Depth>2</Depth>
          <Modification>2022-01-03T13:03:44.0060696+01:00</Modification>
          <Name>Therry</Name>
          <Company>Network</Company>
          <Email>jenexistepas@gmail.com</Email>
          <Relation>Other</Relation>
          <Surname>BARRET</Surname>
        </Component>
      </Components>
      <LastUsed>true</LastUsed>
    </Component>
  </Components>
  <LastUsed>false</LastUsed>
</Component>

```

### Binary


Exemple de persistance binaire : 


```binary
@	Component.type˜Folder7http://schemas.datacontract.org/2004/07/Model.Composite	i)http://www.w3.org/2001/XMLSchema-instance@Creation™!2022-01-03T12:58:10.2568634+01:00@Depth@Modification™!2022-01-03T13:01:16.8758656+01:00@Name™Root@
Components@	Component.type˜Contact@Creation™ 2022-01-03T12:59:39.795359+01:00@Depthƒ@Modification™!2022-01-03T12:59:39.7953657+01:00@Name™POULAIN@Company™Isima@Email™Maxime.POULAIN@etu.uca.fr@Relation™Other@Surname™Maxime@	Component.type˜Contact@Creation™!2022-01-03T13:00:50.9634656+01:00@Depthƒ@Modification™!2022-01-03T13:00:50.9634816+01:00@Name™LY@Company™Isima@Email™Sinaro.LY@etu.uca.fr@Relation™Other@Surname™Sinaro@	Component.type˜Folder@Creation™ 2022-01-03T13:01:16.875501+01:00@Depthƒ@Modification™!2022-01-03T13:03:44.0061039+01:00@Name™Copains@
Components@	Component.type˜Contact@Creation™!2022-01-03T13:03:44.0060545+01:00@Depth‰@Modification™!2022-01-03T13:03:44.0060696+01:00@Name™Therry@Company™Network@Email™jenexistepas@gmail.com@Relation™Other@Surname™BARRET@LastUsed‡@LastUsed…

```







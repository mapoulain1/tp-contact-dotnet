﻿using Model.Controller;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Composite
{

    [DataContract]
    public class Folder : Component
    {

        [DataMember]
        public IList<Component> Components { get; private set; }

        [DataMember]
        public bool LastUsed { get; private set; }





        public Folder(string name, int depth = 0) : base(name, depth)
        {
            Components = new List<Component>();
            LastUsed = true;
            Subscribe(new StreamingContext());
        }

        [OnDeserialized]
        private void Subscribe(StreamingContext c)
        {
            ContactManager.GetInstance().Used += Manager_Used;
        }


        private void Manager_Used(Component obj)
        {
            LastUsed = obj == this;
        }

        public void Reload()
        {
            Components = new List<Component>(Components);
        }
        public override void Add(Component component)
        {
            if (component != null)
            {
                Modification = DateTime.Now;
                component.Depth = Depth + 1;
                Components.Add(component);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Depth; i++)
                sb.Append('\t');
            sb.Append(LastUsed ? "* " : "  ");
            sb.Append($"[D] {Name} ({Creation})\n");
            foreach (var item in Components)
            {
                sb.Append(item);
            }
            return sb.ToString();
        }
    }
}

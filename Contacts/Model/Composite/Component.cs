﻿using System;
using System.Runtime.Serialization;

namespace Model.Composite
{
    /// <summary>
    /// Classe abstraite du composite
    /// </summary>
    [DataContract]
    [KnownType(typeof(Folder))]
    [KnownType(typeof(Contact))]
    public abstract class Component
    {

        [DataMember]
        public string Name { get; protected set; }

        [DataMember]
        public DateTime Creation { get; protected set; }

        [DataMember]
        public DateTime Modification { get; protected set; }



        /// <summary>
        /// Profondeur dans l'arborescence
        /// </summary>
        [DataMember]
        public int Depth { get; internal set; }

        public Component(string name, int depth)
        {
            if (name.IndexOfAny(@",/\.".ToCharArray()) >= 0)
            {
                throw new ArgumentException("Folder name can't contain '<>:\" /\\|?'");
            }
            Name = name;
            Creation = DateTime.Now;
            Modification = DateTime.Now;
            Depth = depth;
        }

        /// <summary>
        /// Ajoute un composant
        /// </summary>
        /// <param name="component">le composant</param>
        public abstract void Add(Component component);


    }
}

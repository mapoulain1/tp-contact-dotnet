﻿using Model.Relation;
using System;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace Model.Composite
{
    [DataContract]
    public class Contact : Component
    {
        [DataMember]
        public string Surname { get; protected set; }

        [DataMember]
        public string Email { get; protected set; }

        [DataMember]
        public string Company { get; protected set; }

        [DataMember]
        public RelationType Relation { get; protected set; }


        public Contact(string name, string surname, string email, string company, RelationType? relation = null) : base(name, 0)
        {
            if (!IsValidEmail(email))
                throw new ArgumentException($"Invalid email {email}");
            Surname = surname;
            Email = email;
            Company = company;
            Relation = relation == null ? RelationType.Other : relation.Value;
        }


        public override void Add(Component component) { }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Depth; i++)
                sb.Append('\t');
            sb.Append($"  [C] {Surname} {Name} ({Company}), {Email}, {Relation}, {Creation}\n");
            return sb.ToString();
        }
        private static bool IsValidEmail(string strIn)
        {
            return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

    }
}

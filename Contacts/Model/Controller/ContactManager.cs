﻿using Model.Composite;
using System;
using System.Collections.Generic;

namespace Model.Controller
{
    public class ContactManager
    {
        public Component Root { get; private set; }
        public Component LastUsed { get; private set; }

        public event Action<Component> Used;
        private static ContactManager instance;

        public static ContactManager GetInstance()
        {
            if (instance == null)
            {
                instance = new ContactManager();
            }
            return instance;
        }

        private ContactManager() { }

        public void Init()
        {
            Root = new Folder("Root");
            LastUsed = Root;
        }

        public void Load(Folder root)
        {
            if (root != null)
            {
                Root = root;
                Queue<Folder> queue = new Queue<Folder>();
                queue.Enqueue(Root as Folder);
                while (queue.Count != 0)
                {
                    var peek = queue.Dequeue();
                    peek.Reload();
                    if (peek.LastUsed)
                    {
                        LastUsed = peek;
                        Used?.Invoke(LastUsed);
                        break;
                    }
                    foreach (var child in peek.Components)
                    {
                        if (child.GetType() == typeof(Folder))
                        {
                            queue.Enqueue(child as Folder);
                        }
                    }
                }
            }
        }

        public void Add(Component component)
        {

            LastUsed.Add(component);
            if (component.GetType() == typeof(Folder))
            {
                LastUsed = component;
                Used?.Invoke(LastUsed);
            }

        }


        public bool Select(string folderName)
        {
            Queue<Folder> queue = new Queue<Folder>();
            queue.Enqueue(Root as Folder);
            while (queue.Count != 0)
            {
                var peek = queue.Dequeue();
                if (peek.Name == folderName)
                {
                    LastUsed = peek;
                    Used?.Invoke(LastUsed);
                    return true;
                }
                foreach (var child in peek.Components)
                    if (child.GetType() == typeof(Folder))
                        queue.Enqueue(child as Folder);
            }
            return false;
        }

        public override string ToString()
        {
            return Root.ToString();
        }

    }
}

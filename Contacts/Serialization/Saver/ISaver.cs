﻿using Model.Composite;

namespace Serialization.Saver
{
    public interface ISaver
    {
        public string Extension { get; }

        /// <summary>
        /// Sauvegarde les contacts
        /// </summary>
        /// <param name="filename">le nom du fichier</param>
        /// <param name="password">le mot de passe éventuel</param>
        /// <param name="root">l'arborescence</param>
        public void Save(string filename, string password, Folder root);

    }
}

﻿using Model.Composite;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Serialization.Saver
{
    public class SaverXML : ISaver
    {
        public string Extension => ".xml";

        private XmlWriterSettings WriterSettings { get; }

        public SaverXML()
        {
            WriterSettings = new XmlWriterSettings { Indent = true };
        }
        public void Save(string filename, string password, Folder root)
        {
            try
            {
                filename += Extension;
                DataContractSerializer serializer = new DataContractSerializer(typeof(Component));
                using var stream = new FileStream(filename, FileMode.Create, FileAccess.Write);
                if (password == string.Empty)
                {
                    using var writer = XmlWriter.Create(stream, WriterSettings);
                    serializer.WriteObject(writer, root);
                }
                else
                {
                    var key = Encoding.ASCII.GetBytes(password.PadRight(32));
                    var iv = new byte[16];
                    var encryptor = new AesCryptoServiceProvider().CreateEncryptor(key, iv);
                    using var crypto = new CryptoStream(stream, encryptor, CryptoStreamMode.Write);
                    using var writer = XmlWriter.Create(crypto, WriterSettings);
                    serializer.WriteObject(writer, root);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }
}

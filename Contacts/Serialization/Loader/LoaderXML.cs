﻿using Model.Composite;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Serialization.Loader
{
    public class LoaderXML : ILoader
    {
        public string Extension => ".xml";

        public void Erase(string filename)
        {
            File.Delete(filename + Extension);
        }

        public Folder Load(string filename, string password)
        {
            try
            {
                filename += Extension;
                DataContractSerializer serializer = new DataContractSerializer(typeof(Component));
                using var stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                if (password == string.Empty)
                {
                    using var reader = XmlReader.Create(stream);
                    return serializer.ReadObject(reader) as Folder;
                }
                else
                {
                    var key = Encoding.ASCII.GetBytes(password.PadRight(32));
                    var iv = new byte[16];
                    var encryptor = new AesCryptoServiceProvider().CreateDecryptor(key, iv);
                    using var crypto = new CryptoStream(stream, encryptor, CryptoStreamMode.Read);
                    using var reader = XmlReader.Create(crypto);
                    return serializer.ReadObject(reader) as Folder;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}

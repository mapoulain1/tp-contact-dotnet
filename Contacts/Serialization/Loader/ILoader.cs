﻿using Model.Composite;

namespace Serialization.Loader
{
    public interface ILoader
    {
        public string Extension { get; }

        /// <summary>
        /// Charge une arborescence
        /// </summary>
        /// <param name="filename">le nom du fichier</param>
        /// <param name="password">le mot de passe éventuel</param>
        /// <returns>l'arborescence</returns>
        public Folder Load(string filename, string password);

        /// <summary>
        /// Supprime le fichier
        /// </summary>
        /// <param name="filename">Le fichier</param>
        public void Erase(string filename);
    }
}

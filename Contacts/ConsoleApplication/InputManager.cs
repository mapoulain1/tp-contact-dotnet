﻿using Model.Composite;
using Model.Relation;
using Serialization.Loader;
using Serialization.Saver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication
{
    internal static class InputManager
    {
        public static List<string> Commands;

        private static List<(Command command, string value)> commands;

        static InputManager()
        {
            commands = new List<(Command command, string value)>
            {
                (Command.AddContact, "addcontact"),
                (Command.AddFolder, "addfolder"),
                (Command.Exit, "exit"),
                (Command.Load, "load"),
                (Command.Print, "print"),
                (Command.Save, "save"),
                (Command.Select, "select"),
                (Command.Help, "help"),
                (Command.Unknown, ""),
            };
            Commands = commands.Select(x => x.value).ToList();
            Commands.Remove("");
        }


        internal static Command GetCommand(string input)
        {
            return commands.Find(x => x.value == input.Split(' ')[0]).command;
        }


        internal static string NearestCommand(string input)
        {
            string best = string.Empty;
            int bestDistance = int.MaxValue;
            foreach (var command in commands)
            {
                var distance = DamerauLevenshtein.DamerauLevenshteinDistance(command.value, input);
                if (distance < bestDistance)
                {
                    bestDistance = distance;
                    best = command.value;
                }
            }
            return best;

        }

        internal static Contact GenerateContact(string input)
        {
            var splitted = input.Split(' ');
            if (splitted.Length != 5 && splitted.Length != 6)
                throw new ArgumentException($"Wrong number of arguments to instanciate a contact ({splitted.Length} instead of 5/6)\naddcontact [name] [surname] [email] [compagny] (relation)");
            RelationType? relationType = null;
            try
            {
                relationType = (RelationType)(Enum.Parse(typeof(RelationType), splitted[5]));
            }
            catch (Exception) { }
            return new Contact(splitted[1], splitted[2], splitted[3], splitted[4], relationType);
        }

        internal static Folder GenerateFolder(string input)
        {
            var splitted = input.Split(' ');
            if (splitted.Length < 2)
                throw new ArgumentException($"Wrong number of arguments to instanciate a folder ({splitted.Length} instead of 2)");
            var list = splitted.ToList();
            list.RemoveAt(0);
            return new Folder(string.Join(' ', list));
        }


        internal static string Select(string input)
        {
            var splitted = input.Split(' ');
            if (splitted.Length < 2)
                throw new ArgumentException($"Wrong number of arguments to select a folder ({splitted.Length} instead of 2)");
            var list = splitted.ToList();
            list.RemoveAt(0);
            return string.Join(' ', list);
        }

        internal static ISaver Save(string input)
        {
            var splitted = input.Split(' ');
            if (splitted.Length < 2)
                return new SaverXML();
            var list = splitted.ToList();
            list.RemoveAt(0);
            if (list[0].ToLower() == "bin")
                return new SaverBinary();

            else if (list[0].ToLower() == "xml")
                return new SaverXML();

            throw new ArgumentException($"Unknown type of format for saving ({list[0]}). Please use \"xml\" or \"bin\"");
        }
        internal static ILoader Load(string input)
        {
            var splitted = input.Split(' ');
            if (splitted.Length < 2)
                return new LoaderXML();
            var list = splitted.ToList();
            list.RemoveAt(0);
            if (list[0].ToLower() == "bin")
                return new LoaderBinary();

            else if (list[0].ToLower() == "xml")
                return new LoaderXML();

            throw new ArgumentException($"Unknown type of format for loading ({list[0]}). Please use \"xml\" or \"bin\"");
        }


        internal static string ReadLineSecure()
        {
            var password = string.Empty;
            ConsoleKey key;
            do
            {
                var keyInfo = Console.ReadKey(intercept: true);
                key = keyInfo.Key;

                if (key == ConsoleKey.Backspace && password.Length > 0)
                {
                    Console.Write("\b \b");
                    password = password[0..^1];
                }
                else if (!char.IsControl(keyInfo.KeyChar))
                {
                    Console.Write("*");
                    password += keyInfo.KeyChar;
                }
            } while (key != ConsoleKey.Enter);
            Console.WriteLine();
            return password;
        }

    }
}

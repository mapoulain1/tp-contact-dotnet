﻿namespace ConsoleApplication
{
    internal enum Command
    {
        Unknown,
        Exit,
        Print,
        Load,
        Save,
        AddFolder,
        AddContact,
        Select,
        Help,
    }
}

﻿using Configuration;
using Model.Composite;
using Model.Controller;
using System;
using System.Runtime.Serialization;

namespace ConsoleApplication
{
    internal class EntryPoint
    {


        static void Main(string[] args)
        {
            HandleApplication();
        }

        private static void HandleApplication()
        {
            ContactManager contactManager = ContactManager.GetInstance();
            IConfigurable configuration = new Properties();
            LoadConfiguration(configuration);
            contactManager.Init();

            Command command = Command.Unknown;
            while (command != Command.Exit)
            {
                string input = Console.ReadLine();
                command = InputManager.GetCommand(input);
                if (command == Command.Unknown)
                {
                    var nearest = InputManager.NearestCommand(input);
                    if (nearest != string.Empty)
                        WriteLine($"Do you mean '{nearest}' ?", ConsoleColor.Blue);
                }
                try
                {

                    switch (command)
                    {
                        case Command.Unknown:
                            WriteLine($"Unknown command : \"{input}\"", ConsoleColor.Red);
                            break;
                        case Command.Exit:
                            WriteLine("Goodbye !", ConsoleColor.Green);
                            break;
                        case Command.Print:
                            WriteLine(contactManager.ToString());
                            break;
                        case Command.Load:
                            Load(contactManager, configuration, input);
                            break;
                        case Command.Save:
                            Save(contactManager, configuration, input);
                            break;
                        case Command.AddFolder:
                            var folder = InputManager.GenerateFolder(input);
                            contactManager.Add(folder);
                            WriteLine($"{folder.Name} added !", ConsoleColor.Green);
                            break;
                        case Command.AddContact:
                            var contact = InputManager.GenerateContact(input);
                            contactManager.Add(contact);
                            WriteLine($"{contact.Surname} {contact.Name} added !", ConsoleColor.Green);
                            break;
                        case Command.Select:
                            var result = contactManager.Select(InputManager.Select(input));
                            WriteLine(result ? "Selected !" : "Not found.", result ? ConsoleColor.Green : ConsoleColor.Red);
                            WriteLine(contactManager.ToString());
                            break;
                        case Command.Help:
                            Help();
                            break;
                    }
                }
                catch (Exception e)
                {
                    WriteLine(e.Message, ConsoleColor.Red);
                }
            }
            SaveConfiguration(configuration);

        }

        private static void Help()
        {
            Console.WriteLine("Contacts : help");
            Console.WriteLine("Commands available : ");
            foreach (var command in InputManager.Commands)
            {
                Console.WriteLine($"\t{command}");
            }
        }

        private static void Save(ContactManager contactManager, IConfigurable configurable, string input)
        {
            try
            {
                var saver = InputManager.Save(input);
                Console.Write("Password : ");
                var password = InputManager.ReadLineSecure();
                saver.Save(configurable.DatabaseFilename, password, contactManager.Root as Folder);
                WriteLine($"Done saving ! ({configurable.DatabaseFilename}{saver.Extension})", ConsoleColor.Green);
            }
            catch (Exception e)
            {
                WriteLine($"{e.Message}", ConsoleColor.Red);
            }
        }

        private static void Load(ContactManager contactManager, IConfigurable configurable, string input)
        {
            try
            {
                var loader = InputManager.Load(input);
                var password = string.Empty;
                var attemps = 0;
                var success = true;

                do
                {
                    try
                    {
                        attemps++;
                        Console.Write($"[{attemps}/3] Password : ");
                        password = InputManager.ReadLineSecure();
                        var root = loader.Load(configurable.DatabaseFilename, password);
                        contactManager.Load(root);
                        success = true;
                    }
                    catch (SerializationException)
                    {
                        success = false;
                        WriteLine("Wrong password.", ConsoleColor.Red);
                    }
                } while (!success && attemps < 3);

                if (success)
                {
                    WriteLine($"Done loading ! ({configurable.DatabaseFilename}{loader.Extension})", ConsoleColor.Green);
                }
                else
                {
                    WriteLine($"File will be erased ({configurable.DatabaseFilename}{loader.Extension})", ConsoleColor.Red);
                    loader.Erase(configurable.DatabaseFilename);
                }
            }
            catch (Exception e)
            {
                WriteLine($"{e.Message}", ConsoleColor.Red);
            }
        }




        private static void WriteLine(string line, ConsoleColor? color = null)
        {
            var oldColor = Console.ForegroundColor;
            if (color != null)
                Console.ForegroundColor = color.Value;
            Console.WriteLine(line);
            Console.ForegroundColor = oldColor;
        }

        private static void LoadConfiguration(IConfigurable configurable)
        {
            WriteLine("Loading configuration from \"config.properties\"");
            try
            {
                configurable.Load("config.properties");
                WriteLine("Done loading configuration !");
            }
            catch (Exception e)
            {
                WriteLine(e.Message, ConsoleColor.Red);
            }
        }


        private static void SaveConfiguration(IConfigurable configurable)
        {
            try
            {
                configurable.Save();
            }
            catch (Exception e)
            {
                WriteLine(e.Message, ConsoleColor.Red);
            }
        }


    }
}

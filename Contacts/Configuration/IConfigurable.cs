﻿namespace Configuration
{
    public interface IConfigurable
    {
        void Load(string filename);
        void Save();
        string DatabaseFilename { get; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Configuration
{
    public class Properties : IConfigurable
    {
        public string Filename { get; set; }

        private Dictionary<string, string> Config { get; set; }

        public string DatabaseFilename
        {
            get
            {
                if (!Config.ContainsKey("database_path"))
                {
                    Config.Add("database_path", Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "contacts"));
                    Save();
                }
                return Config.GetValueOrDefault("database_path");
            }
        }

        public Properties()
        {
            Config = new Dictionary<string, string>();
        }


        public void Load(string filename)
        {
            Filename = filename;
            try
            {
                var lines = File.ReadAllLines(filename);
                for (int i = 0; i < lines.Length; i++)
                {
                    var splitted = lines[i].Split('=');
                    if (splitted.Length != 2)
                        throw new FormatException($"Error reading line {i} of {filename} : \"{lines[i]}\"");
                    try
                    {
                        Config.Add(splitted[0], splitted[1]);
                    }
                    catch (ArgumentException) { }
                }

            }
            catch (Exception)
            {
                _ = DatabaseFilename;
                Save();
            }
        }

        public void Save()
        {
            using StreamWriter file = new StreamWriter(Filename);
            foreach (var prop in Config)
                file.WriteLine($"{prop.Key}={prop.Value}");
            file.Close();
        }



    }
}
